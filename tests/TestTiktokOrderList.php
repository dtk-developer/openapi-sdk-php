<?php
include './vendor/autoload.php';

// 抖音-订单列表
$client = new TiktokOrderList();

$client->setAppKey('xxxxxxxxx');
$client->setAppSecret('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
$client->setVersion('v2.0.0');

$params = [
    "start_time" => '', // 开始时间
    "end_time" => '', // 结束时间
    "order_ids" => '1', // 订单ID列表，多个订单以,分割
    "external_info" => '', // 外部参数
    "page" => 1, // 页码
    "size" => 20, // 每页数量
];

$res = $client->setParams($params)->request();
var_dump($res);