<?php

/**
 * Class PddOauthQuery 拼多多-授权备案-查询是否已经备案
 * String pid 推广位
 * String customParameters 自定义参数
 */
class PddOauthQuery extends DtkClient
{
    protected $pid;
    protected $customParameters;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/api/ppd/authority-query";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['pid', 'custom_parameters'];
    }

    /**
     * @return array
     */
    public function check()
    {
        return ['', true];
    }
}