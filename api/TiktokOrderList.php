<?php

/**
 * Class PddOauthQuery 抖音-订单列表
 * String startTime 开始时间
 * String endTime 结束时间
 * String orderIds 订单ID列表(多个订单ID以,分割)
 * String externalInfo 外部参数
 * Integer page 页码
 * Integer size 每页数量
 */
class TiktokOrderList extends DtkClient
{
    protected $startTime;
    protected $endTime;
    protected $orderIds;
    protected $externalInfo;
    protected $page;
    protected $size;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/api/tiktok/order-list";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['start_time', 'end_time', 'order_ids', 'external_info', 'page', 'size'];
    }

    /**
     * @return array
     */
    public function check()
    {
        return ['', true];
    }
}