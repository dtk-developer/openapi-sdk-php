<?php

/**
 * Class VipPromoteLink 唯品会商品转链
 * Array urlList required 唯品会链接url列表,非联盟链接
 * String chanTag pid
 * String statParam 自定义渠道统计参数
 * String urlGenRequest cps链接生成请求参数
 * String authId 授权id
 */
class VipPromoteLink extends DtkClient
{
    protected $urlList;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/vip/promote/link";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['urlList','chanTag','statParam','urlGenRequest','authId'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->urlList) {
            return ['urlList不能为空！', false];
        }
        return ['', true];
    }
}
