<?php

/**
 * Class VipRefundOrderList 唯品会维权订单列表
 * String authId  授权id
 * Integer pageSize required 分页大小:默认20，最大50
 * String searchType required 查询类型:0-维权发起时间，1-维权完成时间（佣金扣除入账时间），2-佣金扣除结算时间
 * String searchTimeStart  目标时间起始：时间戳，单位毫秒; 当searchType=0时，为维权发起时间， 当searchType=1时，为维权完成时间（佣金扣除入账时间）， 当searchType=2时，为佣金扣除结算时间
 * String searchTimeEnd  目标时间结束：时间戳，单位毫秒; 当searchType=0时，为维权发起时间， 当searchType=1时，为维权完成时间（佣金扣除入账时间）， 当searchType=2时，为佣金扣除结算时间
 * String orderSns  目标订单号集合:当指定订单号集合时，目标时间区间可以不传,否则必须指定目标时间区间
 * Integer page required 当前页
 */
class VipRefundOrderList extends DtkClient
{
    protected $searchType;
    protected $page;
    protected $pageSize;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/vip/refund-order-list";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['authId','pageSize','searchType','searchTimeStart','searchTimeEnd','orderSns','page'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->searchType) {
            return ['searchType不能为空！', false];
        }
        if (!$this->page) {
            return ['page不能为空！', false];
        }
        return ['', true];
    }
}
