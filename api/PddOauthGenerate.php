<?php

/**
 * Class PddOauthGenerate 拼多多-授权备案-创建
 * String pIdList 推广位
 * String customParameters 自定义参数
 */
class PddOauthGenerate extends DtkClient
{
    protected $pIdList;
    protected $customParameters;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/api/ppd/generate-prom-url";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['p_id_list', 'custom_parameters'];
    }

    /**
     * @return array
     */
    public function check()
    {
        return ['', true];
    }
}